class FileLike:
    def __init__(self, content: bytes, name: str):
        self.content = content
        self.name = name

    def read(self):
        return self.content

    def name(self):
        return self.name

    def __getitem__(self, item):
        return self.content.__getitem__(item)