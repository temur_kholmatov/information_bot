from core.texts import *
import telebot


def main_menu_markup():
    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(TABLE_EMOJI, PROGRAM_EMOJI, NAVIGATION_EMOJI)
    markup.add(PARTICIPANT_EMOJI, VIEWER_EMOJI, SOS_EMOJI)
    return markup
