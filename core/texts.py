SOS_EMOJI = '\U0001f198'
TABLE_EMOJI = "\U0001f4ca"
PROGRAM_EMOJI = "\U0001f5d3"
PARTICIPANT_EMOJI = "\U0001f468"
NAVIGATION_EMOJI = "\U0001f5fa"
CITY_EMOJI = "\U0001f3d9"
AREA_EMOJI = "\U0001f3df"
VIEWER_EMOJI = "\U0001f440"
BACK = "\U000021a9"

MENU_TEXT = f"{TABLE_EMOJI} - результаты\n" \
            f"{PROGRAM_EMOJI} - программа\n" \
            f"{NAVIGATION_EMOJI} - навигация\n" \
            f"{PARTICIPANT_EMOJI} - участнику\n" \
            f"{VIEWER_EMOJI} - зрителю\n" \
            f"{SOS_EMOJI} - задать вопрос"

INFO_NOT_AVAILABLE = "Запрашиваемая информация недоступна"


YES = 'Да'
NO = 'Нет'

STATUS_CHOICES = {
    '0': 'Участник',
    '1': 'Тренер',
    '3': 'Судья (Творческая)',
    '4': 'Судья',
    '5': 'Региональный организатор',
    '7': 'Зритель',
}

GROUP_REGISTERED = "Группа успешно зарегистрирована"
GROUP_KEY_INVALID = "Невалидный ключ"

USER_INFO = 'Вы вошли как <b>{}</b>.\nВаш статус - <b>{}</b>.\n'
REGION_INFO = 'Ваш регион - <b>{}</b>.\n'
COMPETITION_INFO = 'Ваше соревнование - <b>{}</b>.\n'
