#################################################
#              Main settings file.              #
# Do not change ANYTHING without authorization. #
#################################################
import logging
from datetime import date

# TOKEN #

TOKEN: str = "YOUR_TOKEN_FROM_BOTFATHER"

# PROXY #

PROXY_SETTINGS = {
    'https': 'socks5://freetelegram.org:freetelegram.org@srv1.freetelegram.org:10000'
}

# RABBITMQ #

RABBITMQ_HOST: str = 'rmq'
RABBITMQ_PORT: int = 5672
RABBITMQ_USERNAME = 'RMQ_USERNAME'
RABBITMQ_PASSWORD = 'RMQ_PASSWORD'

# DATABASE #

DB_HOST = 'pg'
DB_NAME = 'DB_NAME'
DB_USER = 'DB_USERNAME'
DB_PASSWORD = 'DB_PASSWORD'

DB_CONNECTION = f"host='{DB_HOST}' dbname='{DB_NAME}' user={DB_USER} password={DB_PASSWORD}"

# WEBHOOK #

WEBHOOK_HOST = 'WHERE_TO_POINT_WEBHOOK'
WEBHOOK_PORT = 443

WEBHOOK_LISTEN = 'WHERE_TO_LISTEN_WEBHOOK'
WEBHOOK_LISTEN_PORT = 443

WEBHOOK_URL_BASE = f'https://{WEBHOOK_HOST}:{WEBHOOK_PORT}'
WEBHOOK_URL_PATH = f'/{TOKEN}/'

WEBHOOK_SSL_CERT = 'PATH_TO_CERT'
WEBHOOK_SSL_PRIV = 'PATH_TO_PRIVATE_KEY'


SHEETS_API_SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')

LOG_LEVEL = logging.ERROR


def create_logger(logger_name: str = 'bot'):
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.INFO)

    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)

    formatter = logging.Formatter(LOG_FORMAT)
    ch.setFormatter(formatter)

    logger.addHandler(ch)

    return logger


TIME_ZONE = 'Europe/Moscow'
FIRST_DAY_DATE = date(day=22, month=6, year=2018)

CHEATERS_FINDER_FLAG = True
