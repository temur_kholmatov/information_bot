FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./sos_response_microservice/ /src/sos_response_microservice/
CMD [ "python", "./sos_response_microservice/main.py" ]
