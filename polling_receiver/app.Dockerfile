FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./polling_receiver/ /src/polling/
CMD [ "python", "./polling/app.py" ]
