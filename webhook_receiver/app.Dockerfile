FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

EXPOSE 443
EXPOSE 8443

ADD  ./core/ /src/core/

ADD ./webhook_cert.pem /src/webhook_cert.pem
ADD ./webhook_pkey.pem /src/webhook_pkey.pem

ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./webhook_receiver/ /src/webhook_receiver/
CMD [ "python", "./webhook_receiver/app.py" ]
