import psycopg2
import core.settings as s


def find_token(tg_id, user_token):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM users WHERE token = %s', (user_token,))
    # if such token exists update tg_id of user; otherwise return verdict=0
    if len(cursor.fetchall()) > 0:
        cursor.execute('UPDATE users SET tg_id = NULL WHERE tg_id = %s', (tg_id,))
        cursor.execute('UPDATE users SET tg_id = %s, state = 1 WHERE token = %s', (tg_id, user_token))
        connection.commit()
        connection.close()
        return True
    else:
        connection.close()
        return False


def add_alias(tg_id, alias):
    if alias is not None:
        alias = '@' + alias
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM users WHERE tg_id = %s', (tg_id,))
    if len(cursor.fetchall()) == 1:
        cursor.execute('UPDATE users SET alias = %s WHERE tg_id = %s', (alias, tg_id))
    connection.commit()
    connection.close()


def get_region_names_codes():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT code, name FROM users_region ORDER BY code')
    results = cursor.fetchall()
    connection.commit()
    connection.close()
    return results


def register_next_user(tg_id, user_name, alias):
    if alias is not None:
        alias = '@' + alias
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('INSERT INTO users (name, alias, state) VALUES (%s, %s, 0) RETURNING id',
                   (user_name, alias))
    next_user_id = cursor.fetchone()[0]
    connection.commit()
    cursor.execute('UPDATE users SET next_user_id = %s WHERE tg_id = %s', (next_user_id, tg_id))
    connection.commit()
    connection.close()


def register_user_with_status(user_id, user_name, tg_id, alias, status):
    if alias is not None:
        alias = '@' + alias
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    if user_id is not None:  # if person was in db before
        cursor.execute('UPDATE users SET tg_id = %s WHERE id = %s', (tg_id, user_id))
    else:
        cursor.execute(
            'INSERT INTO users (name, tg_id, alias, status, state, old_tg_id) VALUES (%s, %s, %s, %s, 0, %s)',
            (user_name, tg_id, alias, str(status), tg_id))
    connection.commit()
    connection.close()


def set_next_user_status(user_id, status):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('UPDATE users SET status = %s WHERE id = %s', (status, user_id))
    connection.commit()
    connection.close()


def set_user_region(user_id, code):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM users_region WHERE code = %s', (code,))
    if len(cursor.fetchall()) == 0:
        connection.commit()
        connection.close()
        return False
    cursor.execute('SELECT * FROM users WHERE id = %s', (user_id,))
    if len(cursor.fetchall()) == 1:
        cursor.execute(
            'UPDATE users AS u SET region_id = r.id FROM users_region AS r WHERE r.code = %s AND u.id = %s',
            (code, user_id))
    connection.commit()
    connection.close()
    return True


def get_status_by_id(user_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT status FROM users WHERE id = %s', (user_id,))
    result = cursor.fetchone()
    if result is not None:
        result = result[0]
    connection.commit()
    connection.close()
    return result


def get_categories_full_names_ids():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT id, name FROM competition_categories')
    rows = cursor.fetchall()
    result = [{'id': row[0], 'cat_name': row[1]} for row in rows]
    connection.commit()
    connection.close()
    return result


def get_competition_full_names_ids(cat_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT id, name FROM competitions WHERE category_id = %s', (cat_id,))
    rows = cursor.fetchall()
    result = [{'id': row[0], 'comp_name': row[1]} for row in rows]
    connection.commit()
    connection.close()
    return result


def set_competition_by_id(user_id, comp_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM users WHERE id = %s', (user_id,))
    if len(cursor.fetchall()) == 1:
        cursor.execute('UPDATE users SET competition_id = %s WHERE id = %s', (comp_id, user_id))
    connection.commit()
    connection.close()


def delete_next_user_by_id(user_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('UPDATE users SET next_user_id = %s WHERE next_user_id = %s', (None, user_id))
    cursor.execute('DELETE FROM users WHERE id = %s', (user_id,))
    connection.commit()
    connection.close()


def get_id_by_old_tg_id(tg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT id FROM users WHERE old_tg_id = %s', (tg_id,))
    result = cursor.fetchone()
    if result is not None:
        result = result[0]
    connection.commit()
    connection.close()
    return result


def get_next_user_id(tg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT next_user_id FROM users WHERE tg_id = %s', (tg_id,))
    result = cursor.fetchone()
    if result is not None:
        result = result[0]
    connection.commit()
    connection.close()
    return result


def reset_tg_id(tg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('UPDATE users SET tg_id = NULL WHERE tg_id = %s', (tg_id,))
    connection.commit()
    connection.close()


def move_data(user_id, next_user_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT name, alias, status, region_id, competition_id FROM users WHERE id = %s', (next_user_id,))
    res = cursor.fetchone()
    cursor.execute(
        'UPDATE users SET name = %s, alias = %s, status = %s, region_id = %s, competition_id = %s WHERE id = %s',
        (res[0], res[1], res[2], res[3], res[4], user_id))
    connection.commit()
    connection.close()
