FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./registration_microservice/ /src/registration_microservice/
CMD [ "python", "./registration_microservice/main.py" ]
