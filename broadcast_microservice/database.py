import psycopg2

from core import settings as s


def get_text_by_msg_id(msg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT text FROM messenger_message WHERE id = %s', (msg_id,))
    text = cursor.fetchone()[0]
    connection.close()
    return text


def get_all_users():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT tg_id FROM users WHERE tg_id IS NOT NULL')
    users = cursor.fetchall()
    connection.close()
    return users


def get_unregistered_users():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT tg_id FROM users_notregisteredusers WHERE tg_id IS NOT NULL')
    users = cursor.fetchall()
    connection.close()
    return users


def set_sent_all_message(msg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('UPDATE messenger_message SET sent = TRUE WHERE id = %s', (msg_id,))
    connection.commit()
    connection.close()


def get_team_ids_and_text_by_msg_id(msg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT text FROM messenger_messageteams WHERE id = %s', (msg_id,))
    text = cursor.fetchone()[0]
    cursor.execute(
        'SELECT team_id FROM messenger_messageteams_teams WHERE messageteams_id = %s', (msg_id,))
    ids = [i[0] for i in cursor.fetchall()]
    connection.close()
    return ids, text


def get_teammates(team_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f'SELECT user_id FROM users_team_participants WHERE team_id={team_id}')
    teammates = cursor.fetchall()
    connection.close()
    return teammates


def set_sent_team_message(msg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('UPDATE messenger_messageteams SET sent = TRUE WHERE id = %s', (msg_id,))
    connection.commit()
    connection.close()


def get_competition_ids_and_text_by_msg_id(msg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT text FROM messenger_messagecompetitions WHERE id = %s', (msg_id,))
    text = cursor.fetchone()[0]
    cursor.execute(
        'SELECT competition_id FROM messenger_messagecompetitions_competitions WHERE messagecompetitions_id = %s',
        (msg_id,))
    ids = [i[0] for i in cursor.fetchall()]
    connection.close()
    return ids, text


def get_teams_by_competition(competition_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f'SELECT id FROM users_team WHERE competition_id={competition_id}')
    teams = cursor.fetchall()
    connection.close()
    return teams


def set_sent_competition_message(msg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('UPDATE messenger_messagecompetitions SET sent = TRUE WHERE id = %s', (msg_id,))
    connection.commit()
    connection.close()


def get_region_ids_and_text_by_msg_id(msg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT text FROM messenger_messageregions WHERE id = %s', (msg_id,))
    text = cursor.fetchone()[0]
    cursor.execute(
        'SELECT region_id FROM messenger_messageregions_regions WHERE messageregions_id = %s', (msg_id,))
    ids = [i[0] for i in cursor.fetchall()]
    connection.close()
    return ids, text


def get_users_of_region(region_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f'SELECT tg_id FROM users WHERE region_id={region_id} AND tg_id IS NOT NULL')
    people = cursor.fetchall()
    connection.close()
    return people


def set_sent_region_message(msg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('UPDATE messenger_messageregions SET sent = TRUE WHERE id = %s', (msg_id,))
    connection.commit()
    connection.close()


def get_status_and_text_by_msg_id(msg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT status, text FROM messenger_messagestatus WHERE id = %s', (msg_id,))
    row = cursor.fetchone()
    connection.close()
    return row[0], row[1]


def get_users_by_status(status_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f'SELECT tg_id FROM users WHERE status={status_id} AND tg_id IS NOT NULL')
    people = cursor.fetchall()
    connection.close()
    return people


def set_sent_status_message(msg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('UPDATE messenger_messagestatus SET sent = TRUE WHERE id = %s', (msg_id,))
    connection.commit()
    connection.close()


def get_tg_id_and_text_by_msg_id(msg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT user_id, text FROM messenger_personalmessage WHERE id = %s', (msg_id,))
    row = cursor.fetchone()
    connection.close()
    return row[0], row[1]


def get_user_tg_id(user_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f'SELECT tg_id FROM users WHERE id={user_id}')
    tg_id = cursor.fetchall()[0][0]
    connection.close()
    return tg_id


def set_sent_personal_message(msg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('UPDATE messenger_personalmessage SET sent = TRUE WHERE id = %s', (msg_id,))
    connection.commit()
    connection.close()
