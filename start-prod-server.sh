#!/usr/bin/env bash

docker swarm init
read -p "Would you like to continue? (y/n)" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
docker stack deploy --compose-file docker_config/compose-helpers.yml info_bots_main

echo "Wait"
sleep 15

./build-prod.sh
docker stack deploy --compose-file docker_config/compose-prod-microservices.yml info_bots
echo "Completed"

fi
