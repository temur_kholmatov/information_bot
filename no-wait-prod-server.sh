#!/usr/bin/env bash
docker stack rm info_bots
echo "Wait"
sleep 15
docker container prune -f

./build-prod.sh
docker stack deploy --compose-file docker_config/compose-prod-microservices.yml info_bots
echo "Completed"
