CHOOSE_CATEGORY = 'Выберите категорию соревнований: '
CHOOSE_COMPETITION = 'Выберите соревнование: '
CHOOSE_SECTION = 'Выберите раздел: '

CATEGORY = 'Категория соревнования: <b>%s</b>\n'
COMPETITION = 'Соревнование: <b>%s</b>\n'
SECTION = 'Раздел: <b>%s</b>\n'
MY_RESULTS = '<b>Мои результаты:</b>\n'
NO_SECTIONS_AVAILABLE = 'На данный момент нет доступных разделов и туров.'
