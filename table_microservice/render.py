import io

import matplotlib.pyplot as plt
import numpy as np
import six

plt.switch_backend('agg')


def render_table(data, col_width=3.0, row_height=0.625, font_size=14,
                 header_color='#40466e', row_colors=['#f1f1f2', 'w'], edge_color='w',
                 bbox=[0, 0, 1, 1], header_columns=0,
                 ax=None, **kwargs):
    # function gets a table and creates an image figure as a result
    if ax is None:
        size = (np.array(data.shape[::-1]) + np.array([0, 1])) * np.array([col_width, row_height])
        fig, ax = plt.subplots(figsize=size)
        ax.axis('off')

    mpl_table = ax.table(cellText=data.values, bbox=bbox, colLabels=data.columns, **kwargs)

    mpl_table.auto_set_font_size(False)
    mpl_table.set_fontsize(font_size)

    for k, cell in six.iteritems(mpl_table._cells):
        cell.set_edgecolor(edge_color)
        if k[0] == 0 or k[1] < header_columns:
            cell.set_text_props(weight='bold', color='w')
            cell.set_facecolor(header_color)
        else:
            cell.set_facecolor(row_colors[k[0] % len(row_colors)])
    return ax, fig


def image(df):
    # create an image from Figure object saving it in a buffer
    ax, fig = render_table(df, header_columns=0, col_width=2.0, bbox=[-0.01, 0, 1, 1])
    buf = io.BytesIO()
    fig.savefig(buf, format='png', bbox_inches='tight', pad_inches=0)
    buf.seek(0)
    img = buf.getvalue()
    buf.close()
    return img
