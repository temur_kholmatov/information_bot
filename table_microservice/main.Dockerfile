FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./table_microservice/ /src/table_microservice/
CMD [ "python", "./table_microservice/main.py" ]
