FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./schedule_microservice/ /src/schedule_microservice/
CMD [ "python", "./schedule_microservice/main.py" ]
