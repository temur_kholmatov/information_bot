from datetime import datetime

import psycopg2
import pytz

from core import settings as s


def get_all_messages_by_now():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    now = datetime.now(pytz.timezone(s.TIME_ZONE))
    cursor.execute(
        'SELECT id FROM messenger_message WHERE date_time <= %s AND valid = FALSE ORDER BY date_time',
        (now,))
    results = [i[0] for i in cursor.fetchall()]
    connection.close()
    return results


def set_valid_all_messages(results):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    if len(results) > 0:
        msg_ids = ', '.join([str(i) for i in results])
        cursor.execute(
            'UPDATE messenger_message SET valid = TRUE WHERE id IN ({})'.format(msg_ids))
        connection.commit()
    connection.close()


def get_personal_messages_by_now():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    now = datetime.now(pytz.timezone(s.TIME_ZONE))
    cursor.execute(
        'SELECT id FROM messenger_personalmessage WHERE date_time <= %s AND valid = FALSE ORDER BY date_time',
        (now,))
    results = [i[0] for i in cursor.fetchall()]
    connection.close()
    return results


def set_valid_personal_messages(results):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    if len(results) > 0:
        msg_ids = ', '.join([str(i) for i in results])
        cursor.execute(
            'UPDATE messenger_personalmessage SET valid = TRUE WHERE id IN ({})'.format(msg_ids))
        connection.commit()
    connection.close()


def get_team_messages_by_now():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    now = datetime.now(pytz.timezone(s.TIME_ZONE))
    cursor.execute(
        'SELECT id FROM messenger_messageteams WHERE date_time <= %s AND valid = FALSE ORDER BY date_time',
        (now,))
    results = [i[0] for i in cursor.fetchall()]
    connection.close()
    return results


def set_valid_team_messages(results):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    if len(results) > 0:
        msg_ids = ', '.join([str(i) for i in results])
        cursor.execute(
            'UPDATE messenger_messageteams SET valid = TRUE WHERE id IN ({})'.format(msg_ids))
        connection.commit()
    connection.close()


def get_competition_messages_by_now():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    now = datetime.now(pytz.timezone(s.TIME_ZONE))
    cursor.execute(
        'SELECT id FROM messenger_messagecompetitions WHERE date_time <= %s AND valid = FALSE ORDER BY date_time',
        (now,))
    results = [i[0] for i in cursor.fetchall()]
    connection.close()
    return results


def set_valid_competition_messages(results):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    if len(results) > 0:
        msg_ids = ', '.join([str(i) for i in results])
        cursor.execute(
            'UPDATE messenger_messagecompetitions SET valid = TRUE WHERE id IN ({})'.format(msg_ids))
        connection.commit()
    connection.close()


def get_region_messages_by_now():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    now = datetime.now(pytz.timezone(s.TIME_ZONE))
    cursor.execute(
        'SELECT id FROM messenger_messageregions WHERE date_time <= %s AND valid = FALSE ORDER BY date_time',
        (now,))
    results = [i[0] for i in cursor.fetchall()]
    connection.close()
    return results


def set_valid_region_messages(results):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    if len(results) > 0:
        msg_ids = ', '.join([str(i) for i in results])
        cursor.execute(
            'UPDATE messenger_messageregions SET valid = TRUE WHERE id IN ({})'.format(msg_ids))
        connection.commit()
    connection.close()


def get_status_messages_by_now():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    now = datetime.now(pytz.timezone(s.TIME_ZONE))
    cursor.execute(
        'SELECT id FROM messenger_messagestatus WHERE date_time <= %s AND valid = FALSE ORDER BY date_time',
        (now,))
    results = [i[0] for i in cursor.fetchall()]
    connection.close()
    return results


def set_valid_status_messages(results):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    if len(results) > 0:
        msg_ids = ', '.join([str(i) for i in results])
        cursor.execute(
            'UPDATE messenger_messagestatus SET valid = TRUE WHERE id IN ({})'.format(msg_ids))
        connection.commit()
    connection.close()
