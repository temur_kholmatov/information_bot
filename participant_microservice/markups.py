from telebot import types
from core import callbacks as cb
from core import texts as t
from participant_microservice.texts import *


def participant_markup():
    keyboard = types.InlineKeyboardMarkup()
    catering_button = types.InlineKeyboardButton(text=CATERING_BTN_TXT, callback_data=cb.catering_callback)
    housing_button = types.InlineKeyboardButton(text=HOUSING_BTN_TXT, callback_data=cb.housing_callback)
    transport_button = types.InlineKeyboardButton(text=TRANSPORT_BTN_TXT, callback_data=cb.transport_callback)
    attache_button = types.InlineKeyboardButton(text=ATTACHE_BTN_TXT, callback_data=cb.attache_callback)
    authorization_button = types.InlineKeyboardButton(text=AUTHORIZATION_BTN_TXT,
                                                      callback_data=cb.authorization_callback)

    keyboard.add(catering_button, housing_button)
    keyboard.add(transport_button, attache_button)
    keyboard.add(authorization_button)
    return keyboard


def catering_markup():
    keyboard = types.InlineKeyboardMarkup()
    first_day_button = types.InlineKeyboardButton(text=FIRST_DAY_BTN_TXT, callback_data=cb.meal_1_day_callback)
    second_day_button = types.InlineKeyboardButton(text=SECOND_DAY_BTN_TXT, callback_data=cb.meal_2_day_callback)
    third_day_button = types.InlineKeyboardButton(text=THIRD_DAY_BTN_TXT, callback_data=cb.meal_3_day_callback)
    back_button = types.InlineKeyboardButton(text=t.BACK, callback_data=cb.back_participant_callback)

    keyboard.add(first_day_button, second_day_button, third_day_button)
    keyboard.add(back_button)
    return keyboard


def housing_markup():
    keyboard = types.InlineKeyboardMarkup()
    place_button = types.InlineKeyboardButton(text=HOUSING_PLACE_BTN_TXT, callback_data=cb.housing_place_callback)
    contact_button = types.InlineKeyboardButton(text=CONTACT_BTN_TXT, callback_data=cb.housing_contact_callback)
    back_button = types.InlineKeyboardButton(text=t.BACK, callback_data=cb.back_participant_callback)

    keyboard.add(place_button, contact_button)
    keyboard.add(back_button)
    return keyboard


def transport_markup():
    keyboard = types.InlineKeyboardMarkup()
    transfer_button = types.InlineKeyboardButton(text=TRANSFER_BTN_TXT, callback_data=cb.transfer_callback)
    shuttle_button = types.InlineKeyboardButton(text=SHUTTLE_BTN_TXT, callback_data=cb.shuttle_callback)
    taxi_button = types.InlineKeyboardButton(text=TAXI_BTN_TXT, callback_data=cb.taxi_callback)
    back_button = types.InlineKeyboardButton(text=t.BACK, callback_data=cb.back_participant_callback)

    keyboard.add(transfer_button, shuttle_button, taxi_button)
    keyboard.add(back_button)
    return keyboard


def attache_markup():
    keyboard = types.InlineKeyboardMarkup()
    attache_list_button = types.InlineKeyboardButton(text=ATTACHE_LIST_BTN_TXT, callback_data=cb.attache_list_callback)
    back_button = types.InlineKeyboardButton(text=t.BACK, callback_data=cb.back_participant_callback)

    keyboard.add(attache_list_button)
    keyboard.add(back_button)
    return keyboard


def back_markup():
    keyboard = types.InlineKeyboardMarkup()
    back_button = types.InlineKeyboardButton(text=t.BACK, callback_data=cb.back_participant_callback)
    keyboard.add(back_button)
    return keyboard


def accept_authorization_markup():
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(types.InlineKeyboardButton(text=t.YES, callback_data=cb.auth_accept_callback))
    keyboard.add(types.InlineKeyboardButton(text=t.NO, callback_data=cb.auth_decline_callback))
    return keyboard
