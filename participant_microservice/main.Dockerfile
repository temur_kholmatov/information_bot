FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./participant_microservice/ /src/participant_microservice/
CMD [ "python", "./participant_microservice/main.py" ]
