CATERING_BTN_TXT = "Питание"
HOUSING_BTN_TXT = "Проживание"
TRANSPORT_BTN_TXT = "Транспорт"
ATTACHE_BTN_TXT = "Атташе"
AUTHORIZATION_BTN_TXT = "Авторизоваться"

PARTICIPANT_QUESTION = "Выберите расписание"
CATERING_QUESTION = "Выберите день питания"
TRANSPORT_QUESTION = "Выберите транспорт"
HOUSING_QUESTION = "Выберите раздел"
AUTHORIZATION_TEXT = "Вы подтверждаете, что хотите авторизоваться как участник с помощью токена? " \
                     "Предыдущие данные будут удалены."

FIRST_DAY_BTN_TXT = "22 июня"
SECOND_DAY_BTN_TXT = "23 июня"
THIRD_DAY_BTN_TXT = "24 июня"

FIRST_DAY_MSG_TXT = "Расписание на 22 июня"
SECOND_DAY_MSG_TXT = "Расписание на 23 июня"
THIRD_DAY_MSG_TXT = "Расписание на 24 июня"

HOUSING_PLACE_BTN_TXT = "Места проживания"
CONTACT_BTN_TXT = "Контакты"

HOUSING_PLACE_MSG_TXT = "Места проживания"
ATTACHE_MSG_TXT = "Информация об атташе"

TRANSFER_BTN_TXT = "Трансфер"
SHUTTLE_BTN_TXT = "Шаттл"
TAXI_BTN_TXT = "Такси"

ATTACHE_LIST_BTN_TXT = "Список атташе"

SHUTTLE_INFO = "Шаттл:\n"
TRANSFER_INFO = "Трансфер:\n"
