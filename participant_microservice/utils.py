from datetime import datetime
from core.settings import FIRST_DAY_DATE


def determine_day():
    date = datetime.now().day
    FD = FIRST_DAY_DATE.day
    delta = date - FD + 1
    return delta if (delta > 0) and (delta < 4) else 0
