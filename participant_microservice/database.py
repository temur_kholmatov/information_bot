import psycopg2

from core import configs as c
from core import settings as s


def data_getter(feature, tg_id=1, action=-1):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f'SELECT {feature} FROM files_pdffields')
    data = cursor.fetchall()
    if action != -1:
        cursor.execute(f'SELECT status, token FROM users WHERE tg_id={tg_id}')
        user = cursor.fetchall()[0]
        status, token = user[0], user[1]
        has_token = True if (token != "") and (token is not None) else False
        cursor.execute(f'INSERT INTO users_statistic (tg_id, query, datetime, status, has_token) VALUES'
                       f' ({tg_id}, {action}, now(), {status}, {has_token})')
        connection.commit()
    connection.close()
    try:
        return data[0][0]
    except:
        return None


def get_meal_id(day, tg_id):
    return data_getter(f'meal_{day}_day_id', tg_id, c.PARTICIPANT_CATERING)


def get_meal_text(day, tg_id):
    return data_getter(f'meal_{day}_day_text', tg_id, c.PARTICIPANT_CATERING)


def is_meal_picture(day):
    return data_getter(f'meal_{day}_day_is_pct')


def get_housing_id(tg_id):
    return data_getter('housing_id', tg_id, c.PARTICIPANT_HOUSING)


def get_housing_text(tg_id):
    return data_getter('housing_text', tg_id, c.PARTICIPANT_HOUSING)


def is_housing_picture():
    return data_getter('housing_is_pct')


def get_contacts(tg_id):
    return data_getter('contacts', tg_id, c.PARTICIPANT_HOUSING)


def get_attache(tg_id):
    return data_getter('attache', tg_id, c.PARTICIPANT_ATTACHE)


def get_personal_meal(tg_id, day):
    if day == 0:
        return ""
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    try:
        cursor.execute(f'SELECT id FROM users WHERE tg_id={tg_id}')
        user_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT team_id FROM users_team_participants WHERE user_id={user_id}')
        team_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT competition_id FROM users_team WHERE id={team_id}')
        competition_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT meal_id FROM competitions WHERE id={competition_id}')
        meal_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT info_{day}_day FROM info_meal WHERE id={meal_id}')
        meal = cursor.fetchall()[0][0]
        connection.close()
    except:
        connection.close()
        return ""
    if meal is None:
        return ""
    return meal


def get_personal_housing(tg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    try:
        cursor.execute(f'SELECT region_id FROM users WHERE tg_id={tg_id}')
        region_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT housing_id FROM users_region WHERE id={region_id}')
        housing_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT info FROM info_housing WHERE id={housing_id}')
        info = cursor.fetchall()[0][0]
        connection.close()
    except:
        connection.close()
        return ""
    if info is None:
        return ""
    return info


def get_personal_taxi(tg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()

    cursor.execute(f'SELECT status, token FROM users WHERE tg_id={tg_id}')
    user = cursor.fetchall()[0]
    status, token = user[0], user[1]
    has_token = True if (token != "") and (token is not None) else False
    cursor.execute(f'INSERT INTO users_statistic (tg_id, query, datetime, status, has_token) VALUES'
                   f' ({tg_id}, {c.PARTICIPANT_TRANSPORT}, now(), {status}, {has_token})')
    connection.commit()

    try:
        cursor.execute(f'SELECT region_id FROM users WHERE tg_id={tg_id}')
        region_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT transport_id FROM users_region WHERE id={region_id}')
        transport_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT taxi FROM info_transport WHERE id={transport_id}')
        info = cursor.fetchall()[0][0]
        connection.close()
    except:
        connection.close()
        return ""

    if info is None:
        return ""
    return info


def get_personal_transport(tg_id, day):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()

    cursor.execute(f'SELECT status, token FROM users WHERE tg_id={tg_id}')
    user = cursor.fetchall()[0]
    status, token = user[0], user[1]
    has_token = True if (token != "") and (token is not None) else False
    cursor.execute(f'INSERT INTO users_statistic (tg_id, query, datetime, status, has_token) VALUES'
                   f' ({tg_id}, {c.PARTICIPANT_TRANSPORT}, now(), {status}, {has_token})')
    connection.commit()

    if day == 0:
        connection.close()
        return "", ""
    try:
        cursor.execute(f'SELECT region_id FROM users WHERE tg_id={tg_id}')
        region_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT transport_id FROM users_region WHERE id={region_id}')
        transport_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT transfer_{day},shuttle_{day} FROM info_transport WHERE id={transport_id}')
        info = cursor.fetchall()[0]
        transfer_info, shuttle_info = info[0], info[1]
        connection.close()
    except:
        connection.close()
        return "", ""
    if transfer_info is None:
        transfer_info = ""
    if shuttle_info is None:
        shuttle_info = ""

    return transfer_info, shuttle_info


def get_personal_attache(tg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    try:
        cursor.execute(f'SELECT region_id FROM users WHERE tg_id={tg_id}')
        region_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT attache_id FROM users_region WHERE id={region_id}')
        attache_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT info FROM info_attache WHERE id={attache_id}')
        info = cursor.fetchall()[0][0]
        connection.close()
    except:
        connection.close()
        return ""
    if info is None:
        return ""
    return info
