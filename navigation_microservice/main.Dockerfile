FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./navigation_microservice/ /src/navigation_microservice/
CMD [ "python", "./navigation_microservice/main.py" ]
