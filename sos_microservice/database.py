import psycopg2
from core import settings as s
from sos_microservice.texts import status as st


def save_msg(message):
    text = message.text
    text = text.replace("'", '"')
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f'SELECT id FROM users WHERE tg_id={message.chat.id}')
    user_id = cursor.fetchall()[0][0]
    cursor.execute('INSERT INTO questions_question (user_id, question, sent) VALUES (%s, %s, false)', (user_id, text))
    connection.commit()
    connection.close()


def get_groups_id():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT tg_id FROM users_jurygroup')
    groups_id = cursor.fetchall()
    connection.close()
    return groups_id


def get_message_to_group(message):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()

    cursor.execute(f'SELECT name,alias,status,region_id,competition_id FROM users WHERE tg_id={message.chat.id}')
    info = cursor.fetchall()[0]
    name = info[0]
    alias = info[1]
    status = st[info[2]]
    region_id = info[3]
    competition_id = info[4]
    if alias is None:
        alias = ""


    try:
        cursor.execute(f'SELECT name FROM users_region WHERE id={region_id}')
        region = cursor.fetchall()[0][0]
    except:
        region = ""


    try:
        cursor.execute(f'SELECT name FROM competitions WHERE id={competition_id}')
        competition = cursor.fetchall()[0][0]
    except:
        competition = ""

    connection.close()

    msg = f"{name} {alias}, {status}, {region}, {competition}: *{message.text}*"
    return msg
