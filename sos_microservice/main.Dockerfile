FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./sos_microservice/ /src/sos_microservice/
CMD [ "python", "./sos_microservice/main.py" ]
