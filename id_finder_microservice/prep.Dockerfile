FROM python:3.6

ADD ./requirements.txt ./requirements.txt

RUN pip install --upgrade pip && pip install -r requirements.txt
