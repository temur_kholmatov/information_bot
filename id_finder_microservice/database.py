import psycopg2
from core import settings as s


def set_file_id(feature, pdf_tg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f"UPDATE files_pdffields SET {feature} = '{pdf_tg_id}'")
    connection.commit()
    connection.close()


def get_receiver_id():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute("SELECT receiver_id FROM files_pdffields")
    receiver_id = cursor.fetchall()[0][0]
    cursor.execute(f"SELECT tg_id FROM users WHERE id={receiver_id}")
    tg_id = cursor.fetchall()[0][0]
    connection.close()
    return tg_id


def is_picture(feature):
    feature = feature.replace('id', 'is_pct')
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f"SELECT {feature} FROM files_pdffields")
    is_pct = cursor.fetchall()[0][0]
    connection.close()
    return is_pct
