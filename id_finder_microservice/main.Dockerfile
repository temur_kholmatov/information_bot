FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./id_finder_microservice/ /src/id_finder_microservice/
CMD [ "python", "./id_finder_microservice/main.py" ]
